<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Eagle Financial</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	 <style>
            html, body {
                height: 100%;
				background: url('http://i.imgur.com/m8wqdGZ.jpg') no-repeat center center fixed;
            }
			</style>

</head>
<body>
<div class="container">
    <a href="{{ action('CustomerController@index') }}">Customers</a> |
    <a href="{{ action('StockController@index') }}">Stocks</a> |
    <a href="{{ action('InvestmentController@index') }}">Investments</a> |
	<a href="{{ action('BondController@index') }}">MutualFunds</a>
</div>
<hr>
<div class="container">
    @yield('content')
</div>
</body>
</html>
